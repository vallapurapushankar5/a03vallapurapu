# A03 server.js

This is the place where you can buy toycars.
user can send message from contact page.

## How to use

Open a command window in your c:\44563\A03Vallapurapu folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node server.js
```

Point your browser to `http://localhost:8081`. 
