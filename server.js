var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var http = require('http').Server(app);

// set up the view engine

app.use(express.static(__dirname + '/views'));
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// create an array to manage our entries
var entries = [];
app.locals.entries = entries;

// set up the http request logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname + '/assets/')));
app.get("/", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/Home.html'));
});

app.get("/contact", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/Contact.html'));
});

app.get("/toycar", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/toycar.html'));
});

app.get("/guestbook", function (request, response) {
  response.render("index");
});

// manage our entries
// http GET (default and /new-entry)
// app.get("/", function (request, response) {
//   response.render("index");
// });
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// set up the logger
// GETS
// POSTS
app.post("/contact", function (req, res) {
  var api_key = 'key-b3c4d84030f68be6ad6ba1991174a03a';
  var domain = 'sandbox64bd44fbdf034e5589c4b241b1b85ca1.mailgun.org';
  var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });

  var data = {
    from: 'shankar <postmaster@sandbox64bd44fbdf034e5589c4b241b1b85ca1.mailgun.org>',
    to: 'vallapurapushankar5@gmail.com',
    subject: req.body.name,
    text: req.body.num
  };

  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if (!error)
      res.send("Mail Sent");
    else
      res.send("Mail not Sent");


  });
});
// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/");
});
// 404
// 404
app.use(function (request, response) {
  response.status(404).render("404");
});





// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});